---
layout: post
title:  "About the pigs"
date:   2021-05-06 00:00:00 -0000
categories: scp-localhost honeyPig Snort
---	
As with so many Dev projects if you spend enough time on something it mutates. HoneyPig is a short honeypot script. The original concept was to listen to unsolicited traffic on external facing TCP ports and emit <a href='https://www.snort.org/'>Snort</a> rules to copy/paste. Small...for the R Pi. Python because it can do anything.

The object oriented python, complete with *little piggy* naming convention quickly found itsself in half a dozen other projects, some related by data provanance, others unique.  
WarPig (Black Sabbath reference) leverages honeyPig's data to fuzz. It was a fork of the violent side of honeyPig, distinguishing passive listening from more aggresive actions. Ultimately it was perfect to enrich honeyPig by running dns, geo and hostname lookups as a separate operation. WarPig was written to replay port scans, at least the first packet after the handshake.

PerimeterPig monitors MAC addresses on Wifi and Bluetooth using Scapy. Once it has samples it can compair Friends, Watch and Alert lists firing messages when it's Rf space has been invaded. To do: spawn listeners on other devices and other frequencies (HackRf and tire guages).

The sillyness aside there is a common thread. The pig object is an easy metaphor for the flow in the scripts. They are object oriented pigs.

```Python3
class pig:
    def __init__(a_pig, name):
        a_pig.name = name
        a_pig.wentToMarket = False
        a_pig.stayedHome = False
 ```
 
 ```Python3
        a_pig.pcap = False
        a_pig.mens_rea = False
```

```Python3
mainPig = pig('babe')
mainPig.db = 'data/Bastards.db'
mainPig.mens_rea = b'\x00' * (int(0xffffff) * 2)
```

```Python3
def sendRick(this_little_piggy):
    if this_little_piggy.mens_rea :
        for roll in rroll:
            #
```

